# ColorPicker

Pick a color and get back RGB, hex and CMY values to use in your apps. Made for the 2018 Xojo #JustCode Challenge.